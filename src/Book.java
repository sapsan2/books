import java.util.ArrayList;

public class Book {
    private String book;
    private String autor;

    public Book(String book, String autor) {
        this.book = book;
        this.autor = autor;
    }

    public Book() {
    }

    public String getBook() {
        return book;
    }

    public void setBook(String book) {
        this.book = book;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }
}
