import java.time.LocalDateTime;

public class Reader {
    private String displayName;
    private Integer ticketsNumber;
    private String department;
    private String birthDay;
    private Integer phoneNumber;

    public Reader(String displayName, Integer ticketsNumber, String department, String birthDay, Integer phoneNumber) {
        this.displayName = displayName;
        this.ticketsNumber = ticketsNumber;
        this.department = department;
        this.birthDay = birthDay;
        this.phoneNumber = phoneNumber;
    }

    public Reader() {
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public Integer getTicketsNumber() {
        return ticketsNumber;
    }

    public void setTicketsNumber(Integer ticketsNumber) {
        this.ticketsNumber = ticketsNumber;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(String  birthDay) {
        this.birthDay = birthDay;
    }

    public Integer getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(Integer phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public String toString() {
        return
                "displayName='" + displayName + '\'' +
                ", ticketsNumber=" + ticketsNumber +
                ", department='" + department + '\'' +
                ", birthDay='" + birthDay + '\'' +
                ", phoneNumber=" + phoneNumber;
    }
}
