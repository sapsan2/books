import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void showBooks(ArrayList<Book> books) {
        for (int i = 0; i < books.size(); i++) {
            System.out.println(i + 1 + ": " + books.get(i).getBook() + " - " + books.get(i).getAutor());
        }
    }

    public static void takeBook(ArrayList<Book> books, Reader reader, Scanner scanner) {
        boolean isRun = true;
        int x;

        while (isRun) {
            x = scanner.nextInt() - 1;
            if (x == 98) {
                isRun = false;
            } else {
                System.out.println(reader.getDisplayName() + " взял книгу: " + books.get(x).getBook() + " - " + books.get(x).getAutor());
            }
        }
        System.out.println();
    }

    public static void returnBook(ArrayList<Book> books, Reader reader, Scanner scanner) {
        boolean isRun = true;
        int y;

        while (isRun) {
            y = scanner.nextInt() - 1;
            if (y == 98) {
                isRun = false;
            } else {
                System.out.println(reader.getDisplayName() + " вернул книгу: " + books.get(y).getBook() + " - " + books.get(y).getAutor());
            }
        }
        System.out.println();
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        Reader readerOne = new Reader("Nurtas", 921, "Technical Faculty", "22-10-1996", 772255);
        Reader readerTwo = new Reader("John", 113, "Technical Faculty", "22-12-1990", 112233);

        Book bookOne = new Book("Гарри Поттер", "Джоан Роулинг");
        Book bookTwo = new Book("Гензель и Гретель", "Братья Гримм");
        Book bookThree = new Book("Портрет дориана грея", "Оскар Уайльд");
        Book bookFour = new Book("унесенные ветром", "Маргарет Митчелл");

        ArrayList<Book> books = new ArrayList<>();

        books.add(bookOne);
        books.add(bookTwo);
        books.add(bookThree);
        books.add(bookFour);

        System.out.println(readerOne.getDisplayName() + " - Какую кингу хотите взять?");
        showBooks(books);
        takeBook(books, readerOne, scanner);
        System.out.println(readerTwo.getDisplayName() + " - Какую кингу хотите взять?");
        showBooks(books);
        takeBook(books, readerTwo, scanner);

        System.out.println(readerOne.getDisplayName() + " - Какую кингу хотите вернуть?");
        showBooks(books);
        returnBook(books, readerOne, scanner);
        System.out.println(readerTwo.getDisplayName() + " - Какую кингу хотите вернуть?");
        showBooks(books);
        returnBook(books, readerTwo, scanner);
    }
}